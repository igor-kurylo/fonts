import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';

import {FontsService} from './shared/services/fonts.service';
import {PaginationService} from './shared/services/pagination.service';
import {HistoryService} from './shared/services/history.service';

import {AppComponent} from './app.component';
import {FontComponent} from './font/font.component';
import {TextComponent} from './text/text.component';
import {PaginationComponent} from './pagination/pagination.component';
import {OptionsComponent} from './options/options.component';
import {HistoryComponent} from './history/history.component';

import {MaterializeModule} from 'angular2-materialize';

@NgModule({
    declarations: [
        AppComponent,
        FontComponent,
        TextComponent,
        PaginationComponent,
        OptionsComponent,
        HistoryComponent
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        FormsModule,
        MaterializeModule
    ],
    providers: [FontsService,
        PaginationService,
        HistoryService],
    bootstrap: [AppComponent]
})
export class AppModule {
}
