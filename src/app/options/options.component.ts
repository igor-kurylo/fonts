import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {FontsService} from '../shared/services/fonts.service';
import {PaginationService} from '../shared/services/pagination.service';
import {HistoryService} from '../shared/services/history.service';


@Component({
    selector: 'app-options',
    templateUrl: './options.component.html',
    styleUrls: ['./options.component.css']
})
export class OptionsComponent implements OnInit, OnDestroy {

    @Input() showOptionsBlock;

    searchText = {};
    sortType = '';
    searchList = [];
    showDataList = false;
    allFonts;
    inputSearchOne = '';
    inputSearchTwo = '';
    twoSearchError;

    fontSize = '';
    minSize = 16;
    maxSize = 60;
    range: number;
    step: number;

    constructor(private fontsService: FontsService,
                private paginationService: PaginationService,
                private historyService: HistoryService) {
    }

    ngOnInit() {
        this.fontsService.searchList$.subscribe((showList: string[]) => {
            this.searchList = showList;
            showList.length !== 0 ? this.showDataList = true : this.showDataList = false;
        });

        this.fontsService.setError$.subscribe((err) => {
            if (err['type'] === 'twoSearchError') {
                this.twoSearchError = err['val'];
            }
        });

        this.historyService.resetSearchOneInput$.subscribe((value: string) => this.inputSearchOne = value);
        this.closePopUp();
        this.range = this.maxSize - this.minSize;
    }

    /**
     * search font by name
     */
    searchMethod(): void {
        this.searchText['val'] = this.inputSearchOne;
        this.searchText['input'] = 'searchOne';
        this.historyService.inputSearchOne = this.inputSearchOne;
        this.fontsService.searchFonts(this.searchText);
    }

    /**
     * search the font by name
     */
    searchTheFont(): void {
        this.searchText['val'] = this.inputSearchTwo;
        this.searchText['input'] = 'searchTwo';
        this.fontsService.searchFonts(this.searchText);
    }

    /**
     * select sorting type
     */
    selectItem(): void {
        if (this.inputSearchTwo) {
            this.inputSearchTwo = '';
            this.showDataList = false;
        }
        this.inputSearchOne = '';
        this.searchMethod();
        this.fontsService.sortType(this.sortType);
    }

    /**
     * go to the font after search and set style
     * @param {string} fontName
     * @param event
     */
    goToFont(fontName: string, event) {
        event.stopPropagation();
        this.historyService.getHistoryList(fontName);
        this.allFonts = this.fontsService.allFonts;
        this.allFonts.forEach((item, index) => {
            if (item.family === fontName) {
                this.paginationService.changePage(Math.ceil((index + 1) / 12));
            }
        });
        this.historyService.setStyleActive(fontName);
        this.inputSearchTwo = '';
        this.showDataList = false;
    }

    /**
     * close pop-up window with fonts
     */
    closePopUp(): void {
        document.querySelector('body').onclick = () => {
            if (this.inputSearchTwo) {
                this.inputSearchTwo = '';
                this.showDataList = false;
                this.twoSearchError = false;
            }
        };
    }

    /**
     * clear filter and go to start page
     */
    clearFilters(): void {
        if (this.inputSearchOne) {
            this.inputSearchOne = '';
            this.historyService.searchFontsAgain({input: 'searchOne', val: this.inputSearchOne});
        } else if (this.inputSearchTwo) {
            this.inputSearchTwo = '';
            this.showDataList = false;
            this.twoSearchError = false;
        }
        if (this.sortType) {
            this.sortType = '';
            this.fontsService.sortType(this.sortType);
        }
        if (this.fontSize) {
            this.fontSize = '';
            this.fontsService.fontSizeMove('inherit');
        }
        this.paginationService.changePage(1);
    }

    /**
     * change font size
     * @param event
     */
    sliderClickMove(event): void {
        const rangeWidth = (document.querySelector(`#${event.target.id}`) as HTMLElement).offsetWidth;
        this.step = rangeWidth / this.range;
        if (event.offsetX > 0 && event.offsetX <= rangeWidth) {
            this.fontSize = Math.round((event.offsetX / this.step) + this.minSize) + 'px';
            this.fontsService.fontSizeMove(this.fontSize);
        } else if (event.offsetX < 0) {
            this.fontsService.fontSizeMove(`${this.minSize}px`);
        } else if (event.offsetX > rangeWidth) {
            this.fontsService.fontSizeMove(`${this.maxSize}px`);
        }
    }

    ngOnDestroy() {
        this.fontsService.searchList$.unsubscribe();
        this.fontsService.setError$.unsubscribe();
    }

}