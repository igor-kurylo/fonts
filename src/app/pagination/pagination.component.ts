import {Component, OnInit} from '@angular/core';
import {PaginationService} from '../shared/services/pagination.service';

@Component({
    selector: 'app-pagination',
    templateUrl: './pagination.component.html',
    styleUrls: ['./pagination.component.css']
})
export class PaginationComponent implements OnInit {

    pages = [];
    pageActive = 1;
    startPage = 1;
    endPage = 7;
    maxLength;

    constructor(private paginationService: PaginationService) {
    }

    ngOnInit() {
        this.paginationService.changePage$.subscribe(
            (page: number) => this.getPage(page)
        );

        this.paginationService.totalPages$.subscribe(
            total => {
                this.maxLength = total;
                total < 7 ? this.endPage = +total : this.endPage = 7;
                this.rewritePages(1);
            }
        );
    }

    /**
     * Rewrite array with pagination visible pages
     * @param {number} page
     */
    rewritePages(page: number) {
        this.pages = [];
        for (let i = this.startPage; i <= this.endPage; i++) {
            this.pages.push(i);
        }
        if (this.maxLength > 7) {
            this.pages[0] = 1; // first element
            this.pages[this.pages.length - 1] = this.maxLength; // last element

            if (page > 3) {
                this.pages[1] = '...';
            }
            if (page < this.maxLength - 2) {
                this.pages[this.pages.length - 2] = '...';
            }
        }
    }

    /**
     * set previous page
     */
    prev() {
        if (this.pageActive !== 1) {
            this.getPage(this.pageActive - 1);
        }
    }

    /**
     * set next page
     */
    next() {
        if (this.pageActive !== this.maxLength) {
            this.getPage(this.pageActive + 1);
        }
    }

    /**
     * definition start and end pages
     * @param {number} page
     */
    getPage(page: number) {
        if (page !== this.pageActive && typeof page !== 'string') {
            if (page <= 4) {
                this.startPage = 1;
                this.endPage = 7;
                this.rewritePages(page);
            } else if (page + 2 >= this.maxLength) {
                this.startPage = this.maxLength - 6;
                this.endPage = this.maxLength;
                this.rewritePages(page);
            } else {
                this.startPage = page - 3;
                this.endPage = page + 3;
                this.rewritePages(page);
            }
            this.pageActive = page;
            this.paginationService.changePage(page);
        }
    }
}

