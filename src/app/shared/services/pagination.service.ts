import {Injectable} from '@angular/core';
import {Subject} from 'rxjs/Rx';

@Injectable()
export class PaginationService {

    changePage$ = new Subject();
    totalPages$ = new Subject();

    constructor() {
    }

    /**
     * change current page
     * @param {number} num
     */
    changePage(num: number) {
        this.changePage$.next(num);
    }

    /**
     * change total pages
     * @param {number} total
     */
    changeTotalPages(total: number) {
        this.totalPages$.next(total);
    }
}
