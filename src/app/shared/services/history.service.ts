import {Injectable} from '@angular/core';
import {Subject} from 'rxjs/Subject';


@Injectable()
export class HistoryService {

    getHistoryList$ = new Subject();
    getAllFontsActive$ = new Subject();
    searchFontsAgain$ = new Subject();
    resetSearchOneInput$ = new Subject();
    setStyleActive$ = new Subject();
    inputSearchOne = '';

    constructor() {
    }

    /**
     * transfer font family selected font
     * @param {string} item
     */
    getHistoryList(item: string) {
        this.getHistoryList$.next(item);
    }

    /**
     * transfer actual font name list
     * @param {string[]} list
     */
    getAllFontsActive(list: string[]) {
        this.getAllFontsActive$.next(list);
    }

    /**
     * call searchFonts method
     * @param {{input: string, val: "string"}} searchText
     */
    searchFontsAgain(searchText) {
        this.searchFontsAgain$.next(searchText);
    }

    /**
     * reset first search input after transition to history item
     * @param value
     */
    resetSearchOneInput(value: any) {
        this.resetSearchOneInput$.next(value);
    }

    /**
     * set style for active font
     * @param font
     */
    setStyleActive(font) {
        this.setStyleActive$.next(font);
    }

}