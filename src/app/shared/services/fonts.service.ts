import {Injectable} from '@angular/core';
import {Subject} from '../../../../node_modules/rxjs/Rx';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class FontsService {

    searchFonts$ = new Subject();
    setError$ = new Subject();
    sortType$ = new Subject();
    searchList$ = new Subject();
    clickOnFont$ = new Subject();
    fontSizeMove$ = new Subject();
    allFonts;

    constructor(private http: HttpClient) {}

    /**
     * transfer data
     * @param {val: string, input: string} searchText
     */
    searchFonts(searchText) {
        this.searchFonts$.next(searchText);
    }

    /**
     * transfer error
     * @param {{type: string, val: boolean}} err
     */
    setError(err: any) {
        this.setError$.next(err);
    }

    /**
     * transfer selected type for sorting
     * @param {string} select
     */
    sortType(select: string) {
        this.sortType$.next(select);
    }

    /**
     * list actual fonts
     * @param {string[]} list
     */
    searchList(list: string[]) {
        this.searchList$.next(list);
    }

    /**
     * data about active font after click
     * @param {{type: string; family: string}} data
     */
    clickOnFont(data: {type: string, family: string}) {
        this.clickOnFont$.next(data);
    }

    /**
     * transfer selected font size
     * @param {string} size
     */
    fontSizeMove(size: string) {
        this.fontSizeMove$.next(size);
    }

    /**
     * get all fonts
     * @param url
     * @returns {Observable<Object>} return all fonts
     */
    getFonts(url: string): Observable<Object> {
        return this.http.get(url);
    }
}
