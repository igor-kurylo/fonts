export class ErrorInfo {
    constructor(public type: string,
                public value: boolean) {
    }
}