import {Component, OnInit, AfterViewChecked} from '@angular/core';
import {FontsService} from './shared/services/fonts.service';

declare var $: any;

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit, AfterViewChecked {

    serverError;
    oneSearchError;
    showOption = true; // show/hide options mobile
    showHistory = true; // show/hide history mobile

    constructor(private fontsService: FontsService) {
    }

    ngOnInit() {
        this.fontsService.setError$.subscribe((err) => {
            if (err['type'] === 'serverError') {
                this.serverError = err['val'];
            } else if (err['type'] === 'oneSearchError') {
                this.oneSearchError = err['val'];
            }
        });
    }

    ngAfterViewChecked() {
        $('.options-panel select').material_select();
    }

}
