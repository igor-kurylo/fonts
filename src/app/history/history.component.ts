import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {HistoryService} from '../shared/services/history.service';
import {PaginationService} from '../shared/services/pagination.service';
import {FontsService} from '../shared/services/fonts.service';

@Component({
    selector: 'app-history',
    templateUrl: './history.component.html',
    styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnInit, OnDestroy {

    @Input() showHistoryBlock; // show/hide history

    historyFontsName: string[] = [];
    allFontsActive: string[] = [];

    constructor(private fontsService: FontsService,
                private historyService: HistoryService,
                private paginationService: PaginationService) {
    }

    ngOnInit() {
        this.historyService.getHistoryList$.subscribe(item => this.writeHistoryList(item));
        this.historyService.getAllFontsActive$.subscribe((list: string[]) => this.allFontsActive = list);
        // if storage is not empty
        if (sessionStorage.getItem('history')) {
            const historyArrayGet = sessionStorage.getItem('history').split(',');
            this.historyFontsName = historyArrayGet;
        }
    }

    /**
     * write history array and write to sessionStorage
     * @param {string} item
     */
    writeHistoryList(item) {
        if (this.historyFontsName.length === 0) {
            this.historyFontsName.unshift(item);
        } else if (this.historyFontsName.indexOf(item) === -1) {
            this.historyFontsName.unshift(item);
            if (this.historyFontsName.length > 5) {
                this.historyFontsName.pop();
            }
        }

        const historyArraySet = this.historyFontsName.join();
        sessionStorage.setItem('history', historyArraySet);
    }

    /**
     * go to font and active here style
     * @param {string} fontName
     */
    goToPageHistory(fontName: string) {
        // clear inputSearchOne
        if (this.historyService.inputSearchOne) {
            this.historyService.searchFontsAgain({input: 'searchOne', val: ''});
            this.historyService.resetSearchOneInput('');
        }
        this.allFontsActive.forEach((item, index) => {
            if (item === fontName) {
                this.paginationService.changePage(Math.ceil((index + 1) / 12));
            }
        });
        this.historyService.setStyleActive(fontName);
    }

    ngOnDestroy() {
        this.historyService.getHistoryList$.unsubscribe();
        this.historyService.getAllFontsActive$.unsubscribe();
    }

}
