import {Component, OnDestroy, OnInit} from '@angular/core';
import {FontsService} from '../shared/services/fonts.service';

@Component({
    selector: 'app-text',
    templateUrl: './text.component.html',
    styleUrls: ['./text.component.css']
})
export class TextComponent implements OnInit, OnDestroy {

    fontFamily = 'inherit';
    fontSize = 'inherit';
    fontStyle = 'normal';
    fontWeight = 'normal';

    setNormal = true;
    setItalic = false;
    setWeight = false;
    boldDisabled = false;

    constructor(private fontsService: FontsService) {
    }

    ngOnInit() {
        this.fontsService.clickOnFont$.subscribe((data) => this.changeFontEvent(data));
        this.fontsService.fontSizeMove$.subscribe((size: string) => this.fontSize = size);
    }

    /**
     * detected action and view font parameters
     * @param {{type: string; family: string}} data
     */
    changeFontEvent(data) {
        this.boldDisabled = false;
        this.fontFamily = data['family'];
        this.fontStyle = '';
        this.fontWeight = '';
        // click on family
        if (data['type'] === 'click') {
            this.resetDisabledFlag();
        } else if (data['type'] === 'change') { // change options font
            const elem = (document.querySelector('.text-cont') as HTMLElement).style;
            this.fontStyle = elem.fontStyle;
            this.fontWeight = elem.fontWeight;
            // normal or regular
            if (elem.fontStyle === 'normal' || elem.fontStyle === '') {
                this.resetDisabledFlag();
            } else { // italic
                this.setNormal = false;
                this.setItalic = true;
                this.setWeight = false;
            }
            // bold
            if (+elem.fontWeight >= 700) {
                this.boldDisabled = true;
            }
        }
    }

    /**
     * set font-style normal
     */
    setNormalStyle() {
        this.setNormal = true;
        this.setItalic = false;
        this.fontStyle = 'normal';
    }

    /**
     * set font-style italic
     */
    setItalicStyle() {
        this.setNormal = false;
        this.setItalic = true;
        this.fontStyle = 'italic';
    }

    /**
     * set font weight
     */
    setWeightStyle() {
        this.setWeight = !this.setWeight;
        this.setWeight ? this.fontWeight = 'bold' : this.fontWeight = 'normal';
    }

    /**
     * reset disabled flag
     */
    resetDisabledFlag() {
        this.setNormal = true;
        this.setItalic = false;
        this.setWeight = false;
    }

    /**
     * focus on text after click
     */
    textFocus() {
        (document.querySelector('.text-cont') as HTMLElement).focus();
    }

    ngOnDestroy() {
        this.fontsService.clickOnFont$.unsubscribe();
        this.fontsService.fontSizeMove$.unsubscribe();
    }
}
