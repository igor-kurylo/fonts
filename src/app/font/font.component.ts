import {Component, OnInit, OnDestroy} from '@angular/core';

import {FontsService} from '../shared/services/fonts.service';
import {PaginationService} from '../shared/services/pagination.service';
import {HistoryService} from '../shared/services/history.service';

import WebFont from 'webfontloader/webfontloader';

@Component({
    selector: 'app-font',
    templateUrl: './font.component.html',
    styleUrls: ['./font.component.css']
})

export class FontComponent implements OnInit, OnDestroy {

    fonts = {};
    allFonts; // all fonts
    allFontsSearch = []; // all fonts after filtering

    fontsNames = []; // all fonts name
    fontsNamesSearch = []; // all fonts name after filtering
    fontWeightLoad; // type font for loading

    loadFontsName = []; // fonts for load
    showFontsName = []; // fonts for display

    quantityFonts = 12; // quantity fonts on the page

    currentPage: number; // current page
    quantityPages: number; // total pages

    fontSize = 'inherit'; // font size slider
    fontFamilyActive = 'inherit'; // active font for display
    styleValue;

    startSorting = 'alpha';
    url = 'https://www.googleapis.com/webfonts/v1/webfonts?key=AIzaSyBLBeqjz4y-yYybCig6p1PMKnt9g4PLLNU&sort=';

    historyFontsFamilyLoad = []; // array for loading fonts (history list)

    constructor(private fontsService: FontsService,
                private paginationService: PaginationService,
                private historyService: HistoryService) {
    }

    ngOnInit() {
        this.getPage(this.setPage.bind(this), this.startSorting);
        this.paginationService.changePage$.subscribe(num => this.setPage(num));
        this.fontsService.searchFonts$.debounceTime(750).subscribe((searchText) => this.searchFonts(searchText));
        this.fontsService.sortType$.subscribe((select: string) => this.sortingFonts(select));
        this.fontsService.fontSizeMove$.subscribe((size: string) => this.fontSize = size);
        this.historyService.searchFontsAgain$.subscribe((searchText) => this.searchFonts(searchText));
        this.historyService.setStyleActive$.subscribe((font: string) => this.fontFamilyActive = font);

        if (sessionStorage.getItem('history')) {
            const historyArrayGet = sessionStorage.getItem('history').split(',');
            this.historyFontsFamilyLoad = historyArrayGet;
        }
    }

    /**
     * get pages
     * @param {(num: number) => void} params
     * @param {string} sortType
     */
    getPage(params: (num: number) => void, sortType: string) {
        if (!this.fonts[sortType]) {
            this.fontsService
                .getFonts(this.url + sortType)
                .subscribe((fonts) => {
                        this.fontsNames = [];
                        this.fonts[sortType] = fonts['items'];
                        this.allFonts = fonts['items'];
                        this.allFonts.forEach(item => {
                            this.fontsNames.push(item.family);
                        });

                        this.quantityPages = Math.ceil(this.allFonts.length / this.quantityFonts);
                        this.paginationService.changeTotalPages(this.quantityPages);
                    },
                    () => this.fontsService.setError({type: 'serverError', val: true}),
                    () => {
                        params(1);
                        this.historyService.getAllFontsActive(this.fontsNames); // transfer actual fonts
                    }
                );
        } else {
            this.fontsNames = [];
            this.allFonts = this.fonts[sortType];
            this.allFonts.forEach(item => {
                this.fontsNames.push(item.family);
            });
            this.historyService.getAllFontsActive(this.fontsNames); // transfer actual fonts
        }
    }

    /**
     * set current page
     * @param num {number}
     */
    setPage(num): void {
        this.currentPage = num;
        if (this.allFontsSearch.length === 0) {
            this.loadFontsName = this.fontsNames.slice(this.quantityFonts * (this.currentPage - 1),
                this.quantityFonts * this.currentPage);
            this.showFontsName = this.allFonts.slice(this.quantityFonts * (this.currentPage - 1),
                this.quantityFonts * this.currentPage);
        } else {
            this.searchFontsPagination();
        }
        WebFont.load({
            google: {
                families: this.loadFontsName.concat(this.historyFontsFamilyLoad)
            }
        });
    }

    /**
     * two methods of search font
     * @param {{val: string; input: string}} searchText
     */
    searchFonts(searchText) {
        // first search
        if (searchText.input === 'searchOne' && searchText.val !== '') {
            this.fontsNamesSearch = [];
            this.allFontsSearch = this.allFonts.filter(item => {
                return item.family.toLowerCase().indexOf(searchText.val.toLowerCase()) !== -1 &&
                    item.family.toLowerCase().indexOf(searchText.val.toLowerCase()) === 0;
            });

            if (this.allFontsSearch.length > 0) {
                this.allFontsSearch.forEach(item => this.fontsNamesSearch.push(item.family));
                this.paginationService.changePage(1);
                this.fontsService.setError({type: 'oneSearchError', val: false});
            } else {
                this.showFontsName = [];
                this.quantityPages = 1;
                this.paginationService.changeTotalPages(this.quantityPages);
                this.fontsService.setError({type: 'oneSearchError', val: true});
            }
        } else if (searchText.input === 'searchOne' && searchText.val === '') {
            this.allFontsSearch = [];
            this.fontsService.setError({type: 'oneSearchError', val: false});
            this.quantityPages = Math.ceil(this.allFonts.length / this.quantityFonts);
            this.paginationService.changeTotalPages(this.quantityPages);
            this.paginationService.changePage(1);
        }

        // second search
        if (searchText.input === 'searchTwo' && searchText.val !== '') {
            this.fontsNamesSearch = [];
            this.allFontsSearch = this.allFonts.filter(item => {
                return item.family.toLowerCase().indexOf(searchText.val.toLowerCase()) !== -1;
            });

            if (this.allFontsSearch.length > 0) {
                this.fontsService.allFonts = this.allFonts;
                this.allFontsSearch.forEach(item => this.fontsNamesSearch.push(item.family));
                this.fontsService.searchList(this.fontsNamesSearch);
                this.fontsService.setError({type: 'twoSearchError', val: false});
                this.allFontsSearch = [];
            } else {
                // not found font
                this.fontsService.setError({type: 'twoSearchError', val: true});
                this.fontsService.searchList(this.fontsNamesSearch);
            }
        } else if (searchText.input === 'searchTwo' && searchText.val === '') {
            this.fontsService.setError({type: 'twoSearchError', val: false});
            this.fontsNamesSearch = [];
            this.fontsService.searchList(this.fontsNamesSearch);
        }
    }

    /**
     * recounting the total pages and total fonts for loading and display
     */
    searchFontsPagination(): void {
        this.quantityPages = Math.ceil(this.allFontsSearch.length / this.quantityFonts);
        this.paginationService.changeTotalPages(this.quantityPages);
        this.loadFontsName = this.fontsNamesSearch.slice(this.quantityFonts * (this.currentPage - 1),
            this.quantityFonts * this.currentPage);
        this.showFontsName = this.allFontsSearch.slice(this.quantityFonts * (this.currentPage - 1),
            this.quantityFonts * this.currentPage);
    }

    /**
     * sorting fonts by parameters
     * @param {string} select
     */
    sortingFonts(select: string): void {
        if (select) {
            this.getPage(this.setPage.bind(this), select);
        } else {
            this.getPage(this.setPage.bind(this), this.startSorting);
        }
        this.paginationService.changePage(1);
    }

    /**
     * change font type and set style for view
     * @param {string} family
     * @param event
     * @param {number} index
     */
    selectWeight(family: string, event, index: number): void {
        this.fontFamilyActive = family;
        this.historyService.getHistoryList(family);

        if (typeof event === 'object') {
            this.fontWeightLoad = family + `:${event.target.value}`;
            this.styleValue = event.target.value;
        } else {
            this.fontWeightLoad = family + `:${event}`;
            this.styleValue = event;
        }
        const reg = /\d{3,}/;
        const pathToElement = (document.querySelector(`#font-cont-${index} .font-cont__text`) as HTMLElement);

        if (this.styleValue.match(reg) !== null) {
            if (this.styleValue.match(reg)[0] === this.styleValue.match(reg).input) { // weight
                const cssText = `font-family: ${family}; font-style: normal;
                                 font-weight: ${this.styleValue}; font-size: ${this.fontSize}`;
                (document.querySelector('.text-cont') as HTMLElement).style.cssText = cssText;
                pathToElement.style.cssText = cssText;
                this.fontsService.clickOnFont({type: 'change', family: family});
            } else { // weight + italic
                const cssText = `font-family: ${family}; font-style: italic;
                                 font-weight: ${this.styleValue.match(reg)[0]}; font-size: ${this.fontSize}`;
                (document.querySelector('.text-cont') as HTMLElement).style.cssText = cssText;
                pathToElement.style.cssText = cssText;
                this.fontsService.clickOnFont({type: 'change', family: family});
            }
        } else { // regular or italic
            const cssText = `font-family: ${family}; font-style: ${this.styleValue};
                             font-weight: 400; font-size: ${this.fontSize}`;
            (document.querySelector('.text-cont') as HTMLElement).style.cssText = cssText;
            pathToElement.style.cssText = cssText;
            this.fontsService.clickOnFont({type: 'change', family: family});
        }
        WebFont.load({
            google: {
                families: [this.fontWeightLoad]
            }
        });
    }

    /**
     * click on font
     * @param {string} font
     * @param {number} index
     */
    viewFont(font: string, index: number): void {
        if (this.fontFamilyActive !== font) {
            this.fontFamilyActive = font;
            (document.querySelector('.text-cont') as HTMLElement).style
                .cssText = `font-size: ${this.fontSize}`;
            this.fontsService.clickOnFont({type: 'click', family: font});

            const pathToSelect = (document.querySelector(`#font-cont-${index} select`) as HTMLFormElement);
            if (pathToSelect) {
                this.selectWeight(font, pathToSelect.value, index);
            }
            this.historyService.getHistoryList(font);
        }
    }

    ngOnDestroy() {
        this.paginationService.changePage$.unsubscribe();
        this.fontsService.searchFonts$.unsubscribe();
        this.historyService.searchFontsAgain$.unsubscribe();
        this.fontsService.sortType$.unsubscribe();
        this.fontsService.fontSizeMove$.unsubscribe();
    }
}
